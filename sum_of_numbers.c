#include<stdio.h>

int input(int n)
{
    printf("Enter the number of elements to be added:\n");
    scanf("%d",&n);
    return n;
}

int Entries(int n, int arr[])
{
    int i = 0;
    for (i = 0; i<n; i++)
    {
        printf("Enter element %d:\n",i+1);
        scanf("%d",&arr[i]);
    }
}

int Sum(int n, int arr[])
{
    int sum=0;
    for (int i=0; i<n; i++)
    {
        sum += arr[i];
    }
    return sum;
}

int output(int n, int arr[])
{
    printf("%d",Sum(n,arr));
}

int main()
{
    int n = input(n);
    int arr[n];
    Entries(n,arr);
    Sum(n,arr);
    printf("Sum of these %d numbers:\n",n);
    output(n,arr);
    return 0;
}